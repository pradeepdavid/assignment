import React, { useState } from "react";
import { CardContext } from "./Contexts/CardContext";
// import Form from "./Components/RjsForm";
import Cards from "./Components/Cards";

function App() {
  const [card, setCard] = useState([
    {
      id: 1,
      cardNumber: 3423423324,
      name: "test_1",
      validity: "10 / 22",
      cvv: 123
    },
    {
      id: 2,
      cardNumber: 74534524333,
      name: "test_2",
      validity: "8 / 28",
      cvv: 432
    },
    {
      id: 3,
      cardNumber: 4534565766,
      name: "test_3",
      validity: "30 / 21",
      cvv: 777
    },
    {
      id: 4,
      cardNumber: 686787867853,
      name: "test_4",
      validity: "23 / 39",
      cvv: 456
    },
    {
      id: 5,
      cardNumber: 89999983432,
      name: "test_5",
      validity: " 1 / 25",
      cvv: 367
    },
    {
      id: 6,
      cardNumber: 223333333333,
      name: "test_6",
      validity: "18 / 35",
      cvv: 111
    }
  ]);

  const [editCard, setEditCard] = useState();

  return (
    <div className="App">
      <CardContext.Provider value={{ card, setCard, editCard, setEditCard }}>
        <Cards />
      </CardContext.Provider>
    </div>
  );
}

export default App;
