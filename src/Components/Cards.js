import React, { useContext } from "react";
import { styled } from "@material-ui/styles";
import { Grid, Box, Paper, Button } from "@material-ui/core";
import { CardContext } from "../Contexts/CardContext";
import RjsForm from "./RjsForm";

export default function Cards() {
  const { card, setCard, editCard, setEditCard } = useContext(CardContext);

  const deleteCard = (cId) => {
    let DeletedData = card.filter((item) => item.id !== cId);
    setCard(DeletedData);
  };

  const editCardData = (cId) => {
    setEditCard(cId);
  };

  const addNewCard = () => {};

  return (
    <>
      {!editCard ? (
        <Box sx={{ flexGrow: 1 }}>
          <Button onClick={addNewCard} variant="contained" color="success">
            Add New Card
          </Button>
          {card.map((data) => (
            <Grid className="cardList" key={data.id} container spacing={2}>
              <Grid item xs={5} md={5}>
                <p>{data.name}</p>
              </Grid>
              <Grid item xs={4} md={4}></Grid>
              <Grid item xs={3} md={3}>
                <Button
                  onClick={() => deleteCard(data.id)}
                  variant="contained"
                  color="success"
                >
                  DeleteCard
                </Button>
              </Grid>
              <Grid item xs={5} md={5}>
                <p>{data.cardNumber}</p>
              </Grid>
              <Grid item xs={4} md={4}>
                <p>{data.validity}</p>
              </Grid>
              <Grid item xs={3} md={3}>
                <Button
                  onClick={() => editCardData(data.id)}
                  variant="outlined"
                  color="error"
                >
                  EditCard
                </Button>
              </Grid>
            </Grid>
          ))}
        </Box>
      ) : (
        <RjsForm />
      )}
      ;
    </>
  );
}
