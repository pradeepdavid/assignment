import React, { useContext } from "react";
import { Grid, Button } from "@material-ui/core";
import { withTheme } from "react-jsonschema-form";
import { CardContext } from "../Contexts/CardContext";
import { Theme as MuiTheme } from "rjsf-material-ui";

const Form = withTheme(MuiTheme);

const RjsForm = (props) => {
  const { card, setCard, editCard, setEditCard } = useContext(CardContext);

  const onSubmit = ({ formData }) => {
    const { CardNumber, cardHolderName, validThrough, CVV } = formData;
    const newData = card.map((item) => {
      if (item.id === editCard) {
        item.id = editCard;
        item.cardNumber = CardNumber;
        item.name = cardHolderName;
        item.validity = validThrough;
        item.cvv = CVV;
      }
      return item;
    });
    console.log(newData);
    setCard(newData);
    setEditCard("");
  };

  const onBack = () => {
    setEditCard("");
  };

  const getCard = () => {
    return card.filter((item) => item.id === editCard)[0];
  };

  const { cardNumber, name, validity, cvv } = getCard();
  console.log(cardNumber);
  const schema = {
    title: "Credit Card",
    type: "object",
    required: ["CardNumber", "validThrough", "cardHolderName", "CVV"],
    properties: {
      CardNumber: {
        type: "integer",
        title: "Card Number",
        default: cardNumber
      },
      validThrough: {
        type: "string",
        title: "Valid Thru",
        default: validity
      },
      cardHolderName: {
        type: "string",
        title: "Card Holder Name",
        default: name
      },
      CVV: {
        type: "integer",
        title: "CVV",
        default: cvv
      }
    }
  };

  const uiSchema = {
    "ui:rootFieldId": "formOne"
  };

  const formData = {};

  return (
    <>
      <Button onClick={onBack} variant="contained" color="success">
        Back
      </Button>
      <div className="previewCard">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <p>{cardNumber}</p>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <p>Card holder name</p>
            <p>{name}</p>
          </Grid>
          <Grid item xs={6}>
            <p>Valid thru</p>
            <p>{validity}</p>
          </Grid>
        </Grid>
      </div>
      <Form
        schema={schema}
        uiSchema={uiSchema}
        formData={formData}
        onSubmit={onSubmit}
      >
        <div>
          <Button
            variant="contained"
            color="success"
            className="btn btn-primary"
            type="submit"
          >
            Submit
          </Button>
        </div>
      </Form>
    </>
  );
};

export default RjsForm;
